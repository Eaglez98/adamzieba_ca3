package com.mycompany.adamzieba_ca3;

/**
 * Abstract Vehicle superclass from which (LoadCarrier_Vehicle) & 
 * (Car_Vehicle) classes inherit from.
 * @author adamg
 */
public abstract class Vehicle 
{
    /**
     * Attribute declaration
     */
    private String make;
    private String model;
    private int miles; //per kWH
    private int seats;
    private int registration_number; //xx/ll/xx
    private int mileage;
    /*
    private double depot_latitude;
    private double depot_longitude;
*/
}
